#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void segundos(int s)
{   
    int minutos, horas;

    horas=s/3600;
    minutos=(s%3600)/60;
    s=(s%3600)%60;

    printf("horas\tminutos\tsegundos\n");
    printf("%d \t%d \t%d \n", horas, minutos, s);
};