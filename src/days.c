#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void dias(int dias)
{
    int anos, meses;

    anos=dias/365;
    meses=(dias%365)/30;
    dias=(dias%365)%30;

    printf("años\tmeses\tdias\n");
    printf("%d \t %d \t %d\n", anos, meses, dias);
};