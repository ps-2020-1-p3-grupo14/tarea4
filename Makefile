all: bin/estatico bin/dinamico
	export LD_LIBRARY_PATH=./librerias:$LD_LIBRARY_PATH
##################### STATICO ############################3

bin/estatico: obj/archivos.o librerias/libestatic_lib.a
	gcc -static obj/archivos.o -lestatic_lib -L ./librerias/ -o bin/estatico

librerias/libestatic_lib.a: obj/secs.o obj/days.o
	ar rcs librerias/libestatic_lib.a obj/secs.o obj/days.o

obj/archivos.o: src/archivos.c
	gcc -Wall -c -I include/ src/archivos.c -o obj/archivos.o

obj/secs.o: src/secs.c
	gcc -Wall -c src/secs.c -o obj/secs.o

obj/days.o: src/days.c
	gcc -Wall -c src/days.c -o obj/days.o

###################### DINAMICO ############################

bin/dinamico: obj/archivos.o librerias/libdynamic.so
	gcc obj/archivos.o ./librerias/libdynamic.so -o bin/dinamico

librerias/libdynamic.so: src/secs.c src/days.c
	gcc -Wall -shared -fPIC src/secs.c src/days.c -o librerias/libdynamic.so

########################## CLEAR ########################

.PHONY: clean
clean:
	rm obj/*
	rm bin/*
	rm librerias/*